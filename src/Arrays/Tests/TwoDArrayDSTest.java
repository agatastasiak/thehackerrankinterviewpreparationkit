package Arrays.Tests;

import Arrays.TwoDArrayDS;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class TwoDArrayDSTest {

    private int[][] array;

    @BeforeEach
    void setUp() {
        this.array = new int[3][3];
        array[0][0] = 5;
        array[0][1] = 1;
        array[0][2] = 2;
        array[1][0] = 0;
        array[1][1] = -9;
        array[1][2] = -2;
        array[2][0] = -1;
        array[2][1] = 6;
        array[2][2] = 9;
    }

    @Test
    void create2DArray() {
        int[][] arrayTest = TwoDArrayDS.create2DArray(3);
        assertEquals(3, arrayTest.length);
    }

    @Test
    void fulfillArray() {
        TwoDArrayDS.fulfillArray(this.array, -9, 9);
        for (int i = 0; i < this.array.length; i++) {
            for (int j = 0; j < this.array[i].length; j++) {
                assertTrue(this.array[i][j] >= -9 && this.array[i][j] <= 9);
            }
        }
    }

    @Test
    void hoursGlassSum() {
        int expected = TwoDArrayDS.hoursGlassSum(this.array);
        assertEquals(expected, 13);
    }

    @Test
    void printArray() throws FileNotFoundException {
        //Prepare to redirect output
        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        TwoDArrayDS.printArray(this.array);
        assertEquals("5 1 2 " + "\n" + "0 -9 -2 " + "\n" +
                "-1 6 9 " + "\n", outContent.toString());
        //Restore normal output
        PrintStream originalOut = System.out;
        System.setOut(originalOut);
    }
}