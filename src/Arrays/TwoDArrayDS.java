package Arrays;

import java.util.Random;

public class TwoDArrayDS {
    public static void main(String[] args) {
        int[][] array = create2DArray(6);
        fulfillArray(array,-9,9);
        int n = hoursGlassSum(array);
        System.out.println("Sum: " + n);
        printArray(array);
    }

    public static int[][] create2DArray(int size) {
        int[][] array = new int[size][size];
        return array;
    }

    public static void fulfillArray(int[][] array, int minRange, int maxRange) {
        Random r = new Random();
        int range = maxRange - minRange + 1;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                array[i][j] = r.nextInt(range) + minRange;
            }
        }
    }

    public static int hoursGlassSum(int[][] array) {
        // i: rows, j: columns
        int max = Integer.MIN_VALUE;
        for (int i = 1; i < array.length - 1; i++) { // rows -2
            for (int j = 1; j < array[0].length - 1; j++) { // columns -2
                int sum = array[i][j]
                        + array[i - 1][j - 1] + array[i - 1][j] + array[i - 1][j+1]
                        + array[i + 1][j - 1] + array[i + 1][j] + +array[i + 1][j + 1];
                if (sum > max) {
                    max = sum;
                }
            }
        }
        return max;
    }

    public static void printArray(int[][] array){
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.print("\n");
        }
    }
}
